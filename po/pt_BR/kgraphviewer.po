# Translation of kgraphviewer.po to Brazilian Portuguese
# Copyright (C) 2007-2018 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diniz Bortolotto <diniz.bortolotto@gmail.com>, 2007.
# André Marcelo Alvarenga <alvarenga@kde.org>, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2018.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2009, 2020, 2022.
# Frederico Gonçalves Guimarães <frederico@teia.bio.br>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: kgraphviewer\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:26+0000\n"
"PO-Revision-Date: 2022-10-19 15:44-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "André Marcelo Alvarenga, Frederico Gonçalves Guimarães"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alvarenga@kde.org, frederico@teia.bio.br"

#: kgrapheditor.cpp:130 kgraphviewer.cpp:119
#, kde-format
msgid "Session Restore"
msgstr "Restauração da sessão"

#: kgrapheditor.cpp:130
#, kde-format
msgid "Do you want to reload files from previous session?"
msgstr "Deseja recarregar os arquivos da sessão anterior?"

#: kgrapheditor.cpp:146
#, kde-format
msgid "Unable to start"
msgstr "Não foi possível iniciar"

#: kgrapheditor.cpp:146 kgraphviewer.cpp:145
#, kde-format
msgid "Could not find the KGraphViewer part."
msgstr "Não foi possível encontrar o componente KGraphViewer."

#: kgrapheditor.cpp:227 kgraphviewer.cpp:225
#, kde-format
msgid "Closes the current file"
msgstr "Fecha o arquivo atual"

#: kgrapheditor.cpp:238
#, kde-format
msgid "Create a New Vertex"
msgstr "Criar um novo vértice"

#: kgrapheditor.cpp:243
#, kde-format
msgid "Create a New Edge"
msgstr "Criar uma nova aresta"

#: kgrapheditor.cpp:498
#, kde-format
msgid "Save current graph"
msgstr "Salvar o grafo atual"

#: kgrapheditorConfigDialog.cpp:61 kgraphviewerConfigDialog.cpp:72
#, kde-format
msgid "Parsing"
msgstr "Processando"

#: kgrapheditorConfigDialog.cpp:62 kgraphviewerConfigDialog.cpp:73
#, kde-format
msgid "Reloading"
msgstr "Recarregando"

#: kgrapheditorConfigDialog.cpp:63 kgraphviewerConfigDialog.cpp:74
#, kde-format
msgid "Opening"
msgstr "Abrindo"

#: kgrapheditorConfigDialog.cpp:64 kgraphviewerConfigDialog.cpp:75
#, kde-format
msgid "Session Management"
msgstr "Gerenciamento de sessões"

#: KGraphEditorElementTreeWidget.cpp:49 KGraphEditorNodesTreeWidget.cpp:54
#, kde-format
msgid "Add a new attribute"
msgstr "Adicionar um novo atributo"

#: KGraphEditorElementTreeWidget.cpp:55 KGraphEditorNodesTreeWidget.cpp:60
#, kde-format
msgid "Remove this attribute"
msgstr "Remover este atributo"

#: kgrapheditormain.cpp:46
#, kde-format
msgid "KGraphEditor"
msgstr "KGraphEditor"

#: kgrapheditormain.cpp:46
#, kde-format
msgid "A Graphviz DOT graph editor by KDE"
msgstr "Um editor de grafos Graphviz DOT, pela KDE"

#: kgrapheditormain.cpp:46 main.cpp:51
#, kde-format
msgid "(C) 2005-2010 Gaël de Chalendar"
msgstr "(C) 2005-2010 Gaël de Chalendar"

#: kgrapheditormain.cpp:58 main.cpp:69
#, kde-format
msgid "Path or URL to scan"
msgstr "Caminho ou URL para examinar"

#: kgrapheditormain.cpp:58 main.cpp:69
#, kde-format
msgid "[url]"
msgstr "[url]"

#: kgrapheditormain.cpp:83
#, kde-format
msgid ""
"A KGraphEditor window is already open, do you want to open the file in it?"
msgstr ""
"Já existe uma janela aberta do KGraphEditor. Deseja abrir este arquivo nela?"

#: kgrapheditormain.cpp:83 main.cpp:94
#, kde-format
msgid "Opening in new window confirmation"
msgstr "Confirmação de abertura em nova janela"

#: KGraphEditorNodesTreeWidget.cpp:65
#, kde-format
msgid "Remove this node"
msgstr "Remover este nó"

#. i18n: ectx: label, entry (reloadOnChangeMode), group (Notification Messages)
#: kgrapheditorsettings.kcfg:9 kgraphviewersettings.kcfg:9
#, kde-format
msgid "The answer to the question 'File modified, do you want to reload it'"
msgstr "A resposta à pergunta 'O arquivo foi modificado, deseja recarregá-lo'"

#. i18n: ectx: label, entry (parsingMode), group (Notification Messages)
#: kgrapheditorsettings.kcfg:13 kgraphviewersettings.kcfg:13
#, kde-format
msgid ""
"To choose whether to parse using an external command (usually dot) or the "
"graphviz library"
msgstr ""
"Escolher se o processamento é feito por um comando externo (normalmente o "
"dot) ou pela biblioteca graphviz"

#. i18n: ectx: label, entry (openInExistingWindowMode), group (Notification Messages)
#: kgrapheditorsettings.kcfg:17 kgraphviewersettings.kcfg:17
#, kde-format
msgid ""
"The answer to the question 'If another window already exist when launching "
"kgraphviewer, should we open in it, open a new window or ask?'"
msgstr ""
"A resposta à pergunta 'Se já existir outra janela ao iniciar o kgraphviewer, "
"devemos abri-la, abrir uma nova janela ou perguntar?'"

#. i18n: ectx: label, entry (reopenPreviouslyOpenedFilesMode), group (Notification Messages)
#: kgrapheditorsettings.kcfg:21 kgraphviewersettings.kcfg:21
#, kde-format
msgid ""
"The answer to the question 'Should the files that were opened in previous "
"sessions be reopened at startup?'"
msgstr ""
"A resposta à pergunta 'Os arquivos que foram abertos nas sessões anteriores "
"deverão ser reabertos ao iniciar?'"

#. i18n: ectx: label, entry (birdsEyeViewEnabled), group (Notification Messages)
#: kgrapheditorsettings.kcfg:25 kgraphviewersettings.kcfg:25
#: part/kgraphviewer_partsettings.kcfg:9
#, kde-format
msgid "If true, the bird's eye view will be shown if needed."
msgstr "Se for verdadeiro, a visão panorâmica será exibida, caso necessário."

#. i18n: ectx: label, entry (PreviouslyOpenedFiles), group (Notification Messages)
#: kgrapheditorsettings.kcfg:29 kgraphviewersettings.kcfg:29
#, kde-format
msgid "The list of files that were opened in past instances of kgraphviewer"
msgstr ""
"A lista de arquivos que foram abertos em instâncias anteriores do "
"kgraphviewer"

#. i18n: ectx: ToolBar (viewToolBar)
#: kgrapheditorui.rc:23 kgraphviewerui.rc:17
#, kde-format
msgid "View Toolbar"
msgstr "Barra de ferramentas de visualização"

#. i18n: ectx: ToolBar (helpToolBar)
#: kgrapheditorui.rc:26 kgraphviewerui.rc:20
#, kde-format
msgid "Help Toolbar"
msgstr "Barra de ferramentas de ajuda"

#. i18n: ectx: ToolBar (editToolBar)
#: kgrapheditorui.rc:31
#, kde-format
msgid "Edit Toolbar"
msgstr "Barra de ferramentas de edição"

#: kgraphviewer.cpp:118
#, kde-format
msgid "Do you want to reload files from the previous session?"
msgstr "Deseja reabrir os arquivos da sessão anterior?"

#: kgraphviewer.cpp:120
#, kde-format
msgctxt "@action:button"
msgid "Reload"
msgstr "Recarregar"

#: kgraphviewer.cpp:121
#, kde-format
msgctxt "@action:button"
msgid "Do Not Reload"
msgstr "Não recarregar"

#: kgraphviewer.cpp:212
#, kde-format
msgid "Opens a new empty KGraphViewer window."
msgstr "Abre uma nova janela vazia do KGraphViewer."

#: kgraphviewer.cpp:215
#, kde-format
msgid "Shows the file open dialog to choose a Graphviz DOT file to open."
msgstr ""
"Exibe a janela de abertura de arquivos para escolher o arquivo DOT do "
"GraphViz a ser aberto."

#: kgraphviewer.cpp:219
#, kde-format
msgid ""
"This lists files which you have opened recently, and allows you to easily "
"open them again."
msgstr ""
"Isto lista os arquivos abertos recentemente, permitindo reabri-los "
"facilmente."

#: kgraphviewer.cpp:229
#, kde-format
msgid "Quits KGraphViewer."
msgstr "Sai do KGraphViewer."

#: kgraphviewer.cpp:232
#, kde-format
msgid "Shows or hides the status bar."
msgstr "Exibe ou oculta a barra de status."

#: kgraphviewer.cpp:235
#, kde-format
msgid "Toolbar configuration."
msgstr "Configuração da barra de ferramentas."

#: kgraphviewer.cpp:238
#, kde-format
msgid "Main KGraphViewer configuration options."
msgstr "Opções de configuração principais do KGraphViewer."

#: kgraphviewerConfigDialog.cpp:71
#, kde-format
msgid "Appearance"
msgstr "Aparência"

#. i18n: ectx: label, entry (backgroundColor), group (Appearance)
#: kgraphviewersettings.kcfg:34
#, kde-format
msgid "Background color of the graph view."
msgstr "A cor de fundo da visualização do grafo."

#: main.cpp:47
#, kde-format
msgid "KGraphViewer"
msgstr "KGraphViewer"

#: main.cpp:49
#, kde-format
msgid "A Graphviz DOT graph viewer"
msgstr "Um visualizador de grafos em DOT do Graphviz"

#: main.cpp:54
#, kde-format
msgid "Gaël de Chalendar"
msgstr "Gaël de Chalendar"

#: main.cpp:54
#, kde-format
msgid "Original Author and current maintainer"
msgstr "Autor original e mantenedor atual"

#: main.cpp:55
#, kde-format
msgid "Reimar Döffinger"
msgstr "Reimar Döffinger"

#: main.cpp:55 main.cpp:56 main.cpp:57 main.cpp:58
#, kde-format
msgid "Contributor"
msgstr "Colaborador"

#: main.cpp:56
#, kde-format
msgid "Matthias Peinhardt"
msgstr "Matthias Peinhardt"

#: main.cpp:57
#, kde-format
msgid "Sandro Andrade"
msgstr "Sandro Andrade"

#: main.cpp:58
#, kde-format
msgid "Milian Wolff"
msgstr "Milian Wolff"

#: main.cpp:59
#, kde-format
msgid "Martin Sandsmark"
msgstr "Martin Sandsmark"

#: main.cpp:59
#, kde-format
msgid "Port to KF5"
msgstr "Migração para o KF5"

#: main.cpp:94
#, kde-format
msgid ""
"A KGraphViewer window is already open, where do you want to open this file "
"in the existing window?"
msgstr ""
"Já existe uma janela aberta do KGraphViewer. Onde você quer abrir este "
"arquivo nesta janela?"

#: part/canvasedge.cpp:74
#, kde-format
msgid ""
"%1 -> %2\n"
"label='%3'"
msgstr ""
"%1 -> %2\n"
"etiqueta='%3'"

#: part/canvasedge.cpp:78
#, kde-format
msgid "Remove selected edge(s)"
msgstr "Remover a(s) arestas(s) selecionada(s)"

#: part/canvaselement.cpp:92
#, kde-format
msgid "Remove selected element(s)"
msgstr "Remover o(s) elemento(s) selecionado(s)"

#: part/canvasnode.cpp:64
#, kde-format
msgid ""
"id='%1'\n"
"label='%2'"
msgstr ""
"ID='%1'\n"
"etiqueta='%2'"

#: part/dotgraph.cpp:265 part/dotgraph.cpp:268 part/dotgraph.cpp:271
#: part/dotgraph.cpp:274 part/dotgraph.cpp:277 part/dotgraph.cpp:280
#, kde-format
msgid "Layout process failed"
msgstr "Falha no processo de layout"

#: part/dotgraph.cpp:265
#, kde-format
msgid "Unable to start %1."
msgstr "Não foi possível iniciar %1."

#: part/dotgraph.cpp:268
#, kde-format
msgid "%1 crashed."
msgstr "%1 apresentou problema."

#: part/dotgraph.cpp:271
#, kde-format
msgid "%1 timed out."
msgstr "%1 expirou o tempo limite."

#: part/dotgraph.cpp:274
#, kde-format
msgid "Was not able to write data to the %1 process."
msgstr "Não foi possível gravar dados no processo %1."

#: part/dotgraph.cpp:277
#, kde-format
msgid "Was not able to read data from the %1 process."
msgstr "Não foi possível ler dados do processo %1."

#: part/dotgraph.cpp:280
#, kde-format
msgid "Unknown error running %1."
msgstr "Erro desconhecido ao executar %1."

#: part/dotgraphview.cpp:421
#, kde-format
msgid "Select Layout Algo"
msgstr "Selecione o algoritmo de layout"

#: part/dotgraphview.cpp:424
#, kde-format
msgid "Dot"
msgstr "Dot"

#: part/dotgraphview.cpp:425
#, kde-format
msgid "Layout the graph using the dot program."
msgstr "Modelar o grafo usando o programa dot."

#: part/dotgraphview.cpp:429
#, kde-format
msgid "Neato"
msgstr "Neato"

#: part/dotgraphview.cpp:430
#, kde-format
msgid "Layout the graph using the neato program."
msgstr "Modelar o grafo usando o programa neato."

#: part/dotgraphview.cpp:434
#, kde-format
msgid "Twopi"
msgstr "Twopi"

#: part/dotgraphview.cpp:435
#, kde-format
msgid "Layout the graph using the twopi program."
msgstr "Modelar o grafo usando o programa twopi."

#: part/dotgraphview.cpp:439
#, kde-format
msgid "Fdp"
msgstr "Fdp"

#: part/dotgraphview.cpp:440
#, kde-format
msgid "Layout the graph using the fdp program."
msgstr "Modelar o grafo usando o programa fdp."

#: part/dotgraphview.cpp:444
#, kde-format
msgid "Circo"
msgstr "Circo"

#: part/dotgraphview.cpp:445
#, kde-format
msgid "Layout the graph using the circo program."
msgstr "Modelar o grafo usando o programa circo."

#: part/dotgraphview.cpp:457
#, kde-format
msgid "Choose a Graphviz layout algorithm or edit your own one."
msgstr "Escolha um algoritmo de layout do GraphViz ou edite o seu próprio."

#: part/dotgraphview.cpp:459
#, kde-format
msgid ""
"Choose a Graphviz layout algorithm or type in your own command that will "
"generate a graph in the xdot format on its standard output. For example, to "
"manually specify the <tt>G</tt> option to the dot command, type in: <tt>dot -"
"Gname=MyGraphName -Txdot </tt>"
msgstr ""
"Escolha um algoritmo de layout do GraphViz ou escreva o seu próprio comando, "
"para gerar um grafo no formato xdot na saída padrão. Por exemplo, para "
"especificar manualmente a opção <tt>G</tt> no comando dot, digite: <tt>dot -"
"Gname=NomeDoGrafo -Txdot </tt>"

#: part/dotgraphview.cpp:465
#, kde-format
msgid "Layout"
msgstr "Layout"

#: part/dotgraphview.cpp:467
#, kde-format
msgid "Specify layout command"
msgstr "Especificar o comando de layout"

#: part/dotgraphview.cpp:468
#, kde-format
msgid ""
"Specify yourself the layout command to use. Given a dot file, it should "
"produce an xdot file on its standard output."
msgstr ""
"Especifique você mesmo o comando de layout a ser usado. Fornecido um arquivo "
"dot, ele deve produzir um arquivo xdot na saída padrão."

#: part/dotgraphview.cpp:469
#, kde-format
msgid "Reset layout command to default"
msgstr "Restaurar o comando de layout para o padrão"

#: part/dotgraphview.cpp:470
#, kde-format
msgid ""
"Resets the layout command to use to the default depending on the graph type "
"(directed or not)."
msgstr ""
"Restaura o comando de layout a ser usado para o padrão, dependendo do tipo "
"de grafo (direcional ou não)."

#: part/dotgraphview.cpp:472
#, kde-format
msgid "Zoom In"
msgstr "Aumentar zoom"

#: part/dotgraphview.cpp:473
#, kde-format
msgid "Zoom Out"
msgstr "Diminuir zoom"

#: part/dotgraphview.cpp:477
#, kde-format
msgid "Export Graph"
msgstr "Exportar o grafo"

#: part/dotgraphview.cpp:479
#, kde-format
msgid "Allows the graph to be exported in another format."
msgstr "Permite que o grafo seja exportado em outro formato."

#: part/dotgraphview.cpp:481
#, kde-format
msgid ""
"Use the Export Graph menu to export the graph in another format. There is "
"currently only one export format supported: as a PNG image."
msgstr ""
"Use o menu Exportar Grafo para exportar o grafo em outro formato. "
"Atualmente, só existe suporte para um formato de exportação: imagem PNG."

#: part/dotgraphview.cpp:485
#, kde-format
msgid "As Image..."
msgstr "Como imagem..."

#: part/dotgraphview.cpp:486
#, kde-format
msgid "Export the graph to an image file."
msgstr "Exporta o grafo para um arquivo de imagem."

#: part/dotgraphview.cpp:494
#, kde-format
msgid "Enable Bird's-eye View"
msgstr "Ativar a visão panorâmica"

#: part/dotgraphview.cpp:497
#, kde-format
msgid "Enables or disables the Bird's-eye View"
msgstr "Habilita ou desabilita a visão panorâmica"

#: part/dotgraphview.cpp:502
#, kde-format
msgid "Birds-eye View"
msgstr "Visão panorâmica"

#: part/dotgraphview.cpp:503
#, kde-format
msgid "Allows the Bird's-eye View to be setup."
msgstr "Permite que a visão panorâmica seja configurada."

#: part/dotgraphview.cpp:507
#, kde-format
msgid "Top Left"
msgstr "Superior esquerdo"

#: part/dotgraphview.cpp:508
#, kde-format
msgid "Puts the Bird's-eye View in the top-left corner."
msgstr "Coloca a visão panorâmica no canto superior esquerdo."

#: part/dotgraphview.cpp:512
#, kde-format
msgid "Top Right"
msgstr "Superior direito"

#: part/dotgraphview.cpp:513
#, kde-format
msgid "Puts the Bird's-eye View in the top-right corner."
msgstr "Coloca a visão panorâmica no canto superior direito."

#: part/dotgraphview.cpp:517
#, kde-format
msgid "Bottom Left"
msgstr "Inferior esquerdo"

#: part/dotgraphview.cpp:518
#, kde-format
msgid "Puts the Bird's-eye View in the bottom-left corner."
msgstr "Coloca a visão panorâmica no canto inferior esquerdo."

#: part/dotgraphview.cpp:522
#, kde-format
msgid "Bottom Right"
msgstr "Inferior direito"

#: part/dotgraphview.cpp:523
#, kde-format
msgid "Puts the Bird's-eye View in the bottom-right corner."
msgstr "Coloca a visão panorâmica no canto inferior direito."

#: part/dotgraphview.cpp:527
#, kde-format
msgid "Automatic"
msgstr "Automático"

#: part/dotgraphview.cpp:528
#, kde-format
msgid ""
"Let KGraphViewer automatically choose the position of the Bird's-eye View."
msgstr ""
"Permite que o KGraphViewer escolha automaticamente a posição da visão "
"panorâmica."

#: part/dotgraphview.cpp:575
#, kde-format
msgid "Select file"
msgstr "Selecione o arquivo"

#: part/dotgraphview.cpp:589
#, kde-format
msgid "Graph SVG Generated by KGraphViewer"
msgstr "Grafo SVG gerado pelo KGraphViewer"

#: part/dotgraphview.cpp:590
#, kde-format
msgid "Graph SVG Generated by KGraphViewer."
msgstr "Grafo SVG gerado pelo KGraphViewer."

#: part/dotgraphview.cpp:637
#, kde-format
msgid ""
"<h1>Graphviz DOT format graph visualization</h1><p>If the graph is larger "
"than the widget area, an overview panner is shown in one edge. Choose "
"through the context menu if the optimal position of this overview should be "
"automatically computed or put it where you want.</p><h2>How to work with it?"
"</h2><ul><li>To move the graph, you can:  <ul>    <li>click & drag it</"
"li>    <li>use the elevators</li>    <li>press the arrows keys</li>    "
"<li>click somewhere in the panner view</li>    <li>use the mouse wheel (up "
"and down with no modifier, left and right with the <Alt> key pressed)</"
"li>    <li>or click & drag the panner view</li>  </ul></li><li>To zoom, you "
"can either use the zoom in and zoom out toolbar buttons, or click on the "
"<Shift> key while rolling your mouse wheel.</li><li>Try the contextual menu "
"(usually by right-clicking) to discover other possibilities.</li><li>Try the "
"<tt>Print preview</tt> or the <tt>Page setup</tt> buttons to explore the "
"printing options.</li></ul>"
msgstr ""
"<h1>Visualização do grafos no formato DOT do GraphViz</h1><p>Se o grafo for "
"maior que a área do widget, aparecerá um painel de visão panorâmica em um "
"dos cantos. Escolha através do menu de contexto se a melhor posição dessa "
"visão deve ser definida automaticamente ou manualmente.</p> <h2>Como "
"trabalhar com ele?</h2><ul><li>Para mover o grafo, você pode:  <ul>    "
"<li>clicar e arrastá-lo</li>    <li>usar os elevadores</li>    "
"<li>pressionar  as teclas de seta</li>    <li>clicar em algum lugar do "
"painel</li>    <li>usar a roda do mouse (para cima ou baixo, sem qualquer "
"modificador, ou para a esquerda e direita com a tecla <Alt> pressionada)</"
"li>    <li>ou clicar e arrastar a área do painel</li>  </ul></li><li>Para "
"alterar o zoom, você pode usar tanto os botões de zoom na barra de "
"ferramentas, quanto apertar a tecla <Shift>, enquanto desloca a roda do "
"mouse.</li><li>Tente o menu de contexto (normalmente com o botão direito do "
"mouse) para descobrir outras possibilidades.</li><li>Experimente os itens "
"<tt>Visualizar impressão</tt> ou <tt>Configuração da página</tt> para "
"explorar as opções de impressão.</li></ul>"

#: part/dotgraphview.cpp:790
#, kde-format
msgid "no graph loaded"
msgstr "nenhum grafo carregado"

#: part/dotgraphview.cpp:906 part/dotgraphview.cpp:927
#: part/dotgraphview.cpp:974
#, kde-format
msgid "graph %1 is getting loaded..."
msgstr "o grafo %1 está sendo carregado..."

#: part/dotgraphview.cpp:914
#, kde-format
msgid "error parsing file %1"
msgstr "erro ao processar o arquivo %1"

#: part/dotgraphview.cpp:1615
#, kde-format
msgid "Reload Confirmation"
msgstr "Confirmação de recarga"

#: part/dotgraphview.cpp:1615
#, kde-format
msgid ""
"The file %1 has been modified on disk.\n"
"Do you want to reload it?"
msgstr ""
"O arquivo %1 foi modificado no disco.\n"
"Deseja recarregá-lo?"

#: part/dotgraphview.cpp:1701
#, kde-format
msgid "Layout Command"
msgstr "Comando de layout"

#: part/dotgraphview.cpp:1702
#, kde-format
msgid ""
"Specify here the command that will be used to layout the graph.\n"
"The command MUST write its results on stdout in xdot format."
msgstr ""
"Especifique aqui o comando que será usado para o layout do grafo.\n"
"O comando DEVERÁ gravar os seus resultados na stdout, no formato xdot."

#: part/dotgraphview.cpp:2155
#, kde-format
msgid "Failed to open %1"
msgstr "Não foi possível abrir %1"

#: part/kgraphviewer_part.cpp:102
#, kde-format
msgid "Print the graph using current page setup settings"
msgstr "Imprimir o grafo usando a configuração atual da página"

#: part/kgraphviewer_part.cpp:106
#, kde-format
msgid "Open the print preview window"
msgstr "Abrir a janela de visualização da impressão"

#: part/kgraphviewer_part.cpp:110
#, kde-format
msgid "Page setup"
msgstr "Configuração da página"

#: part/kgraphviewer_part.cpp:111
#, kde-format
msgid "Opens the Page Setup dialog to allow graph printing to be setup"
msgstr ""
"Abre a janela de configuração da página, para definir como o grafo será "
"impresso"

#: part/kgraphviewer_part.cpp:114
#, kde-format
msgid "Reload the current graph from file"
msgstr "Recarregar o grafo atual a partir do arquivo"

#: part/kgraphviewer_part.cpp:119
#, no-c-format, kde-format
msgid "Zoom in by 10% on the currently viewed graph"
msgstr "Ampliar a visualização do grafo atual em 10%"

#: part/kgraphviewer_part.cpp:124
#, no-c-format, kde-format
msgid "Zoom out by 10% from the currently viewed graph"
msgstr "Reduzir a visualização do grafo atual em 10%"

#. i18n: ectx: Menu (file)
#: part/kgraphviewer_part.rc:4
#, kde-format
msgid "&File"
msgstr "&Arquivo"

#. i18n: ectx: Menu (view)
#: part/kgraphviewer_part.rc:11
#, kde-format
msgid "&View"
msgstr "E&xibir"

#: part/KgvPageLayout.cpp:68
#, kde-format
msgctxt "Page size"
msgid "Screen"
msgstr "Tela"

#: part/KgvPageLayout.cpp:69
#, kde-format
msgctxt "Page size"
msgid "Custom"
msgstr "Personalizado"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#: part/KgvPageLayoutColumnsBase.ui:27
#, kde-format
msgid "Columns:"
msgstr "Colunas:"

#. i18n: ectx: property (text), widget (QLabel, labelSpacing)
#: part/KgvPageLayoutColumnsBase.ui:37
#, kde-format
msgid "Column spacing:"
msgstr "Espaçamento entre colunas:"

#: part/KgvPageLayoutDia.cpp:62
#, kde-format
msgid "Page Preview"
msgstr "Visualização da página"

#: part/KgvPageLayoutDia.cpp:277
#, kde-format
msgid "Page Size & Margins"
msgstr "Tamanho da página e das margens"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox1)
#: part/KgvPageLayoutHeaderBase.ui:17
#, kde-format
msgid "Header"
msgstr "Cabeçalho"

#. i18n: ectx: property (text), widget (QCheckBox, rhFirst)
#: part/KgvPageLayoutHeaderBase.ui:23
#, kde-format
msgid "Different header for the first page"
msgstr "Cabeçalho diferente para a primeira página"

#. i18n: ectx: property (text), widget (QCheckBox, rhEvenOdd)
#: part/KgvPageLayoutHeaderBase.ui:30
#, kde-format
msgid "Different header for even and odd pages"
msgstr "Cabeçalhos diferentes para páginas pares e ímpares"

#. i18n: ectx: property (text), widget (QLabel, textLabel3)
#: part/KgvPageLayoutHeaderBase.ui:55
#, kde-format
msgid "Spacing between header and body:"
msgstr "Espaçamento entre o cabeçalho e o corpo:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox2)
#: part/KgvPageLayoutHeaderBase.ui:77
#, kde-format
msgid "Footer"
msgstr "Rodapé"

#. i18n: ectx: property (text), widget (QCheckBox, rfFirst)
#: part/KgvPageLayoutHeaderBase.ui:83
#, kde-format
msgid "Different footer for the first page"
msgstr "Rodapé diferente para a primeira página"

#. i18n: ectx: property (text), widget (QCheckBox, rfEvenOdd)
#: part/KgvPageLayoutHeaderBase.ui:90
#, kde-format
msgid "Different footer for even and odd pages"
msgstr "Rodapés diferentes para páginas pares e ímpares"

#. i18n: ectx: property (text), widget (QLabel, textLabel4)
#: part/KgvPageLayoutHeaderBase.ui:115
#, kde-format
msgid "Spacing between footer and body:"
msgstr "Espaçamento entre o rodapé e o corpo:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox3)
#: part/KgvPageLayoutHeaderBase.ui:137
#, kde-format
msgid "Footnote/Endnote"
msgstr "Nota de rodapé/Nota de fim"

#. i18n: ectx: property (text), widget (QLabel, textLabel5)
#: part/KgvPageLayoutHeaderBase.ui:159
#, kde-format
msgid "Spacing between footnote and body:"
msgstr "Espaçamento entre a nota de rodapé e o corpo:"

#: part/KgvPageLayoutSize.cpp:62
#, kde-format
msgid "Unit:"
msgstr "Unidade:"

#: part/KgvPageLayoutSize.cpp:75
#, kde-format
msgid "All values are given in %1."
msgstr "Todos os valores em %1."

#: part/KgvPageLayoutSize.cpp:80
#, kde-format
msgid "Page Size"
msgstr "Tamanho da página"

#: part/KgvPageLayoutSize.cpp:89
#, kde-format
msgid "&Size:"
msgstr "&Tamanho:"

#: part/KgvPageLayoutSize.cpp:110
#, kde-format
msgid "&Width:"
msgstr "&Largura:"

#: part/KgvPageLayoutSize.cpp:121
#, kde-format
msgid "&Height:"
msgstr "&Altura:"

#: part/KgvPageLayoutSize.cpp:135
#, kde-format
msgid "Orientation"
msgstr "Orientação"

#: part/KgvPageLayoutSize.cpp:139
#, kde-format
msgid "&Portrait"
msgstr "&Retrato"

#: part/KgvPageLayoutSize.cpp:143
#, kde-format
msgid "La&ndscape"
msgstr "&Paisagem"

#: part/KgvPageLayoutSize.cpp:151
#, kde-format
msgid "Margins"
msgstr "Margens"

#: part/KgvPageLayoutSize.cpp:374 part/KgvPageLayoutSize.cpp:378
#, kde-format
msgid "Page Layout Problem"
msgstr "Problema no layout da página"

#: part/KgvPageLayoutSize.cpp:374
#, kde-format
msgid "The page width is smaller than the left and right margins."
msgstr "A largura da página é menor que as margens esquerda e direita."

#: part/KgvPageLayoutSize.cpp:378
#, kde-format
msgid "The page height is smaller than the top and bottom margins."
msgstr "A altura da página é menor que as margens superior e inferior."

#: part/KgvUnit.cpp:51
#, kde-format
msgid "Millimeters (mm)"
msgstr "Milímetros (mm)"

#: part/KgvUnit.cpp:53
#, kde-format
msgid "Centimeters (cm)"
msgstr "Centímetros (cm)"

#: part/KgvUnit.cpp:55
#, kde-format
msgid "Decimeters (dm)"
msgstr "Decímetros (dm)"

#: part/KgvUnit.cpp:57
#, kde-format
msgid "Inches (in)"
msgstr "Polegadas (pol)"

#: part/KgvUnit.cpp:59
#, kde-format
msgid "Pica (pi)"
msgstr "Paica (pi)"

#: part/KgvUnit.cpp:61
#, kde-format
msgid "Didot (dd)"
msgstr "Didot (dd)"

#: part/KgvUnit.cpp:63
#, kde-format
msgid "Cicero (cc)"
msgstr "Cícero (cc)"

#: part/KgvUnit.cpp:65
#, kde-format
msgid "Points (pt)"
msgstr "Pontos (pt)"

#: part/KgvUnit.cpp:67
#, kde-format
msgid "Error."
msgstr "Erro."

#: part/pannerview.cpp:64
#, kde-format
msgid "View of the complete graph. Click and drag to move the visible part."
msgstr "Visão do grafo inteiro. Clique e arraste para mover a parte visível."

#: part/pannerview.cpp:66
#, kde-format
msgid ""
"<h1>View of the Complete Graph</h1><p>Single clicking somewhere without the "
"red square will move the center of the view to where the mouse was clicked.</"
"p><p>Clicking and dragging within the red square will cause the view to "
"follow the movement.</p>"
msgstr ""
"<h1>Visão do grafo inteiro</h1><p>Clicar em qualquer lugar fora do quadrado "
"vermelho centralizará a visualização na posição onde o mouse foi clicado.</"
"p><p>Clicar e arrastar dentro do quadrado vermelho fará a visão seguir o "
"movimento.</p>"

#: part/simpleprintingcommand.cpp:98
#, kde-format
msgid "Print Preview"
msgstr "Visualizar impressão"

#: part/simpleprintingengine.cpp:200 part/simpleprintpreviewwindow.cpp:219
#: part/simpleprintpreviewwindow.cpp:237
#, kde-format
msgctxt "Page (number) of (total)"
msgid "Page %1 of %2"
msgstr "Página %1 de %2"

#: part/simpleprintingengine.cpp:202
#, kde-format
msgid "Page %1"
msgstr "Página %1"

#: part/simpleprintingpagesetup.cpp:101
#, kde-format
msgid "Print Preview..."
msgstr "Visualizar impressão..."

#: part/simpleprintingpagesetup.cpp:104
#, kde-format
msgid "Font..."
msgstr "Fonte..."

#: part/simpleprintingpagesetup.cpp:105
#, kde-format
msgid "Changes font for title text."
msgstr "Altera a fonte do texto do título."

#: part/simpleprintingpagesetup.cpp:111
#, kde-format
msgid "<qt><h2>Page Setup for Printing Graph \"%1\"</h2></qt>"
msgstr ""
"<qt><h2>Configuração da página para impressão do grafo \"%1\"</h2></qt>"

#: part/simpleprintingpagesetup.cpp:116
#, kde-format
msgid "Saves settings for this setup as default."
msgstr "Salva esta configuração como a padrão."

#: part/simpleprintingpagesetup.cpp:119
#, kde-format
msgid "Adds date and time to the header."
msgstr "Adiciona data e hora ao cabeçalho."

#: part/simpleprintingpagesetup.cpp:120
#, kde-format
msgid "Adds page numbers to the footer."
msgstr "Adiciona números de página ao rodapé."

#: part/simpleprintingpagesetup.cpp:121
#, kde-format
msgid "Adds table borders."
msgstr "Adiciona as bordas da tabela."

#: part/simpleprintingpagesetup.cpp:129
#, kde-format
msgid "Change Page Size and Margins..."
msgstr "Alterar o tamanho e as margens da página..."

#: part/simpleprintingpagesetup.cpp:130
#, kde-format
msgid "Changes page size and margins."
msgstr "Altera o tamanho e as margens da página."

#: part/simpleprintingpagesetup.cpp:187
#, kde-format
msgid "Chosen font looks like this"
msgstr "A fonte escolhida se parece com isso"

#: part/simpleprintingpagesetup.cpp:203
#, kde-format
msgid "Portrait"
msgstr "Retrato"

#: part/simpleprintingpagesetup.cpp:203
#, kde-format
msgid "Landscape"
msgstr "Paisagem"

#: part/simpleprintingpagesetup.cpp:203
#, kde-format
msgid "margins:"
msgstr "margens:"

#. i18n: ectx: property (text), widget (QLabel, textLabel2)
#: part/simpleprintingpagesetupbase.ui:68
#, kde-format
msgid "Pa&ge font:"
msgstr "&Fonte da página:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox1)
#: part/simpleprintingpagesetupbase.ui:97
#, kde-format
msgid "Page Size && Margins"
msgstr "&Tamanho da página e margens"

#. i18n: ectx: property (text), widget (QCheckBox, addTableBordersCheckbox)
#: part/simpleprintingpagesetupbase.ui:122
#, kde-format
msgid "Add &borders"
msgstr "Adicionar &bordas"

#. i18n: ectx: property (text), widget (QCheckBox, addDateTimeCheckbox)
#: part/simpleprintingpagesetupbase.ui:129
#, kde-format
msgid "Add date and time"
msgstr "Adicionar data e hora"

#. i18n: ectx: property (text), widget (QCheckBox, addPageNumbersCheckbox)
#: part/simpleprintingpagesetupbase.ui:152
#, kde-format
msgid "Add page numbers"
msgstr "Adicionar números de página"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox4)
#: part/simpleprintingpagesetupbase.ui:163
#, kde-format
msgid "Number of pages to fit on"
msgstr "Números de páginas a caber"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox2)
#: part/simpleprintingpagesetupbase.ui:195
#, kde-format
msgid "Horizontally"
msgstr "Horizontalmente"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox3)
#: part/simpleprintingpagesetupbase.ui:207
#, kde-format
msgid "Vertically"
msgstr "Verticalmente"

#. i18n: ectx: property (text), widget (QRadioButton, naturalSizeRadioButton)
#: part/simpleprintingpagesetupbase.ui:228
#, kde-format
msgid "&Natural size"
msgstr "Tamanho &natural"

#. i18n: ectx: property (text), widget (QRadioButton, fitToOnePageRadioButton)
#: part/simpleprintingpagesetupbase.ui:235
#, kde-format
msgid "Fit &to one page"
msgstr "Caber em &uma página"

#. i18n: ectx: property (text), widget (QRadioButton, fitToSeveralPagesRadioButton)
#: part/simpleprintingpagesetupbase.ui:242
#, kde-format
msgid "F&it to several pages"
msgstr "Caber em &várias páginas"

#. i18n: ectx: property (text), widget (QLabel, textLabel6)
#: part/simpleprintingpagesetupbase.ui:258
#, kde-format
msgid "Related actions:"
msgstr "Ações relacionadas:"

#: part/simpleprintpreviewwindow.cpp:69
#, kde-format
msgid "%1 - Print Preview - %2"
msgstr "%1 - Visualização da impressão - %2"

#: part/simpleprintpreviewwindow.cpp:84
#, kde-format
msgid "&Page setup"
msgstr "Configuração da &página"

#: part/simpleprintpreviewwindow.cpp:123
#, kde-format
msgid "Previous Page"
msgstr "Página anterior"

#: part/simpleprintpreviewwindow.cpp:133
#, kde-format
msgid "Next Page"
msgstr "Próxima página"

#. i18n: ectx: property (text), widget (QLabel, label)
#: preferencesAppearance.ui:19
#, kde-format
msgid "Default background color:"
msgstr "Cor de fundo padrão:"

#. i18n: ectx: property (text), widget (QLabel, note)
#: preferencesAppearance.ui:51
#, kde-format
msgid "Note: this will be overridden if the graph has its own background color"
msgstr ""
"Observação: esta será substituída se o grafo tiver a sua própria cor de fundo"

#. i18n: ectx: property (title), widget (QGroupBox, openInExistingWindowMode)
#: preferencesOpenInExistingWindow.ui:17
#, kde-format
msgid "Open in existing window"
msgstr "Abrir na janela existente"

#. i18n: ectx: property (text), widget (QRadioButton, yes)
#: preferencesOpenInExistingWindow.ui:23 preferencesReload.ui:26
#: preferencesReopenPreviouslyOpenedFiles.ui:29
#, kde-format
msgid "&Yes"
msgstr "&Sim"

#. i18n: ectx: property (text), widget (QRadioButton, no)
#: preferencesOpenInExistingWindow.ui:30 preferencesReload.ui:33
#: preferencesReopenPreviouslyOpenedFiles.ui:36
#, kde-format
msgid "&No"
msgstr "&Não"

#. i18n: ectx: property (text), widget (QRadioButton, ask)
#: preferencesOpenInExistingWindow.ui:37
#: preferencesReopenPreviouslyOpenedFiles.ui:43
#, kde-format
msgid "As&k"
msgstr "Per&guntar"

#. i18n: ectx: property (title), widget (QGroupBox, parsingMode)
#: preferencesParsing.ui:17
#, kde-format
msgid "Parse using:"
msgstr "Processar usando:"

#. i18n: ectx: property (text), widget (QRadioButton, external)
#: preferencesParsing.ui:26
#, kde-format
msgid "E&xternal command"
msgstr "Comando e&xterno"

#. i18n: ectx: property (text), widget (QRadioButton, internal)
#: preferencesParsing.ui:33
#, kde-format
msgid "Internal &library"
msgstr "Bib&lioteca interna"

#. i18n: ectx: property (title), widget (QGroupBox, reloadOnChangeMode)
#: preferencesReload.ui:17
#, kde-format
msgid "Reload files modified on disk"
msgstr "Recarregar os arquivos modificados no disco"

#. i18n: ectx: property (text), widget (QRadioButton, ask)
#: preferencesReload.ui:40
#, kde-format
msgid "&Ask"
msgstr "Pergunt&ar"

#. i18n: ectx: property (title), widget (QGroupBox, reopenPreviouslyOpenedFilesMode)
#: preferencesReopenPreviouslyOpenedFiles.ui:17
#, kde-format
msgid "Reopen previously opened files"
msgstr "Reabrir arquivos abertos anteriormente"

#~ msgid "Configure the bindings between keys and actions."
#~ msgstr "Configura as associações entre teclas e ações."

#~ msgid "KGraphViewerPart"
#~ msgstr "KGraphViewerPart"

#~ msgid "Graphviz DOT files viewer"
#~ msgstr "Visualizador de arquivos DOT do GraphViz"

#~ msgid "(c) 2005-2006, Gaël de Chalendar <kleag@free.fr>"
#~ msgstr "(c) 2005-2006, Gaël de Chalendar <kleag@free.fr>"
